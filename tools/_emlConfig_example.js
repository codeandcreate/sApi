/**
 * Example _emlConfig for all modules:
 * It requires that all modules are located the same directory as sApi.min.js.
 */

const _emlConfig = {
  'mutationObserver': {                        
    'src': "extensions/s_mutationObserver.js",
    'fns': [
      'add', 'remove'
    ]
  },
  'replaceWith': {
    'src': "extensions/s_replaceWith.js",
    'fns': [
      'mixedNodes', 'iframe', 'image', 'embed', 'script', 'elementInsteadOf'
    ]
  },
  'require': {
    'src': "extensions/s_require.js",
    'fns': [
      'js', 'css', 'object'
    ]
  }
};