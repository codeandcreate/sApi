/**
 * Loads JS or CSS and executes a function after successful loading
 */
$s.extend({
	/**
	 * @typedef $s.require
	 */
	require: 
	{
		_runningRequireOperationsForId: [],
		/**
		 * @param type      type of the script (stylesheet or javascript)
		 * @param source    URL(s) of a/some sources(s)
		 * @param callback  optional callback function
		 * @param clear     drops an existing tag before (re)insert
		 * @typedef $s.require._requireElement
		 * @private
		 */
		_requireElement: function (type, source, callback, clear)
		{
			function realRequireElement(type, source, callback, clear)
			{
				let requireObjectName = undefined, requireApiName = undefined;
				if (typeof source === "object") {
					requireObjectName = source.objectName || undefined;
					requireApiName		= source.apiName || undefined;
					source            = source.url || undefined;
				}
				
				if (
					typeof source === "undefined" ||
					typeof type === "undefined"
				) {
					return false;
				}
				
				const idForLoaderTag  = "sRequire-" + btoa(source);
				let existingElement = null;
				if ($s.require._runningRequireOperationsForId.indexOf(idForLoaderTag) === -1) {
					$s.require._runningRequireOperationsForId.push(idForLoaderTag);
					existingElement = document.getElementById(idForLoaderTag);
				
					if (existingElement !== null && clear === true) {
						existingElement.parentNode.removeChild(existingElement);
						existingElement = null;
					}
				}
				
				if (existingElement === null) {
					"use strict";
				
					let ref        = null;
					let newElement = null;
				
					switch (type) {
						case 'javascript':
							ref            = window.document.getElementsByTagName("script")[0];
							newElement     = window.document.createElement("script");
							newElement.src = source;
							newElement.id  = idForLoaderTag;
							break;
						case 'stylesheet':
							ref             = window.document.getElementsByTagName("link")[0];
							newElement      = window.document.createElement("link");
							newElement.href = source;
							newElement.type = "text/css";
							newElement.rel  = "stylesheet";
							newElement.id   = idForLoaderTag;
							break;
					}
				
					if (newElement === null) {
						return false;
					}
				
					if (callback && typeof (callback) === "function") {
						if (requireObjectName !== undefined) {
							newElement.onload = $s.require.object(requireObjectName, callback);
						} else if (requireApiName !== undefined) {
							newElement.onload = $s.onApiReady(requireApiName, callback);
						} else {
							newElement.onload = callback;
						}
					}
				
					if (ref === null || ref === undefined) {
						window.document.head.appendChild(newElement);
					} else {
						ref.parentNode.insertBefore(newElement, ref);
					}
				} else if (typeof (callback) === "function") {
					if (requireObjectName !== undefined) {
						$s.require.object(requireObjectName, callback);
					} else {
						callback();
					}
				}
				if ($s.require._runningRequireOperationsForId.indexOf(idForLoaderTag) !== -1) {
					$s.require._runningRequireOperationsForId.splice($s.require._runningRequireOperationsForId.indexOf(idForLoaderTag), 1);
				}
			}
			
			/**
			 * Helper function to go through an array of source files and finally, 
			 * if all sources loaded calls the callback.
			 */
			function goThroughSources(type, sources, callback, clear) 
			{
				if (sources.length > 1) {
					realRequireElement(type, sources.splice(0,1)[0], function() {
						goThroughSources(type, sources, callback, clear);
					}, clear);
				} else {
					realRequireElement(type, sources[0], callback, clear)
				}
			}
			
			if (Array.isArray(source)) {
				goThroughSources(type, source, callback, clear);
			} else {
				realRequireElement(type, source, callback, clear);
			}
		},

		/**
		 * Loads a javascript from scriptSource
		 *
		 * @param scriptSource
		 * @param callback
		 * @param clear
		 * @typedef $s.require.js
		 * @private
		 */
		js: function (scriptSource, callback, clear)
		{
			this._requireElement("javascript", scriptSource, callback, clear);
		},

		/**
		 * Loads a stylesheet from sheetSource
		 *
		 * @param sheetSource
		 * @param callback
		 * @param clear
		 * @typedef $s.require.css
		 * @private
		 */
		css: function (sheetSource, callback, clear)
		{
			this._requireElement("stylesheet", sheetSource, callback, clear);
		},

		/**
		 * Waits for an object to be available in window
		 *
		 * @typedef $s.require.object
		 * @param objectName
		 * @param callback
		 * @param useCustomEventListener
		 */
		object: function (objectName, callback, useCustomEventListener)
		{
			/**
			 * if an object is defined outside of window-context, we must check it with eval...
			 */
			try {
				let re = new RegExp("^[a-zA-Z0-9_]+$");
				if (re.exec(objectName) !== null && typeof eval(objectName) === "object") {
					callback();
					return;
				}
			} catch (e) {}

			useCustomEventListener = useCustomEventListener || false;
			if (Object.hasOwn(window, objectName)) {
				callback();
			} else if (useCustomEventListener !== false) {
				$s.ready(function()
				{
					document.body.addEventListener(useCustomEventListener, callback);
				});
			} else {
				setTimeout(function ()
				{
					$s.require.object(objectName, callback)
				}, 100);
			}
		}
	}
});
