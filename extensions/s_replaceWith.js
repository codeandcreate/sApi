/**
* Replaces the current (...) script tag
*/
$s.extend({
/**
	* @typedef $s.replaceWith
	*/
replaceWith: 
	{
	 /**
		* Wrapper for $s.replaceWith.scriptInsteadOf which adds a script to the dom
		*
		* @typedef $s.replaceWith.mixedNodes
		* @param base64Data
		* @param nodeToReplace
		*/
	 mixedNodes: function (base64Data, nodeToReplace) {
		this.elementInsteadOf(nodeToReplace || document.currentScript, undefined, { 'innerHTML': atob(base64Data) })
	 },
	 /**
		* Wrapper for $s.replaceWith.scriptInsteadOf which adds a iFrame to the dom
		*
		* @typedef $s.replaceWith.iframe
		* @param url
		* @param attributes
		* @param nodeToReplace
		*/
	 iframe: function (url, attributes, nodeToReplace) {
		attributes = attributes || {}
		attributes.src = url

		this.elementInsteadOf(nodeToReplace || document.currentScript, 'iframe', attributes)
	 },
	 /**
		* Wrapper for $s.replaceWith.scriptInsteadOf which adds a img to the dom
		*
		* @typedef $s.replaceWith.iframe
		* @param url
		* @param attributes
		* @param nodeToReplace
		*/
	 image: function (url, attributes, nodeToReplace) {
		attributes = attributes || {}
		attributes.src = url

		this.elementInsteadOf(nodeToReplace || document.currentScript, 'img', attributes)
	 },
	 /**
		* Wrapper for $s.replaceWith.scriptInsteadOf which adds a embed to the dom
		*
		* @typedef $s.replaceWith.iframe
		* @param url
		* @param attributes
		* @param nodeToReplace
		*/
	 embed: function (url, attributes, nodeToReplace) {
		attributes = attributes || {}
		attributes.src = url

		this.elementInsteadOf(nodeToReplace || document.currentScript, 'embed', attributes)
	 },
	 /**
		* Wrapper for $s.replaceWith.scriptInsteadOf which uses document.currentScript for "that", automatically.
		*
		* @typedef $s.replaceWith.script
		* @param urlOrCode
		* @param attributes
		* @param nodeToReplace
		*/
	 script: function (urlOrCode, attributes, nodeToReplace) {
		attributes = attributes || {}
		attributes.type = 'text/javascript'
		if (urlOrCode.substring(0, 2) === '//' || urlOrCode.substring(0, 4) === 'http') {
		 attributes.src = urlOrCode
		} else {
		 attributes.textContent = urlOrCode
		}

		this.elementInsteadOf(nodeToReplace || document.currentScript, 'script', attributes)
	 },
	 /**
		* Replaces a javascript tag (that) with another
		* based on urlOrCode and optional attributes for the new tag
		*
		* @typedef $s.replaceWith.scriptInsteadOf
		*/
	 elementInsteadOf: function (that, newElementType, attributes) {
		//if we don't want a container to be inserted, then we need at least a working container
		let _newElementType = newElementType || 'div'
		let nE = document.createElement(_newElementType)
		const _this = this

		if (typeof attributes === 'object') {
		 // add all attributes to the new node. for textContent, innerText and innerHTML we ned some special stuff
		 for (let attributeName in attributes) {
			if (attributes.hasOwnProperty(attributeName)) {
			 switch (attributeName) {
				case 'textContent':
				 nE.textContent = attributes[attributeName]
				 break
				case 'innerText':
				 nE.innerText = attributes[attributeName]
				 break
				case 'innerHTML':
				 nE.innerHTML = attributes[attributeName]
				 // for inner html we must exeute the script tags...
				 let scripts = nE.querySelectorAll('script')
				 if (scripts !== null) {
					// each script will be recreated and readded to the dom
					scripts.forEach(function (defunctScript) {
					 let scriptAttributes = {}
					 for (let i = defunctScript.attributes.length - 1; i >= 0; i--) {
						if (['type', 'data-category'].indexOf(defunctScript.attributes[i].name) === -1) {
						 scriptAttributes[defunctScript.attributes[i].name] = defunctScript.attributes[i].value
						}
					 }
					 scriptAttributes.textContent = defunctScript.textContent
					 _this.elementInsteadOf(defunctScript, 'script', scriptAttributes)
					})
				 }
				 break
				default:
				 nE.setAttribute(attributeName, attributes[attributeName])
			 }
			}
		 }
		}
		if (typeof that === 'object' && that !== null) {
		 if (newElementType === undefined) {
			// if we don't want a container, we just add every child to the dom before the running script
			for (let i = 0; i < nE.childNodes.length; i++) {
			 that.parentNode.insertBefore(nE.childNodes[i].cloneNode(true), that)
			}
		 } else {
			// if we want a container, just add the new node before the running script
			that.parentNode.insertBefore(nE, that)
		 }
		// we don't need the script anymore
		 that.parentNode.removeChild(that)
		} else {
		 // no node to replace? just add to the body
		 document.body.appendChild(nE)
		}
	 }
	}
});