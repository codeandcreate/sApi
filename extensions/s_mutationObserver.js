/**
 * An example of a simple global mutation observer - bound on document.body - to watch for dom changes
 *
 * const _emlConfig = {
 * 	'mutationObserver': {
 *  	'src': "/path/to/your/scripts/s_mutationObserver.js",
 * 		'fns': [
 * 			'add', 'remove'
 * 		]
 * 	},
 *  //...
 * };
 */
$s.extend({
	/**
	* @typedef $s.mutationObserver
	*/
	mutationObserver:
	{
		_registeredCallbacks: {},
		/**
		 * @typedef $s.mutationObserver.add
		 */
		add: function(name, cb)
		{
			if (typeof this._registeredCallbacks[name] !== name) {
				this._registeredCallbacks[name] = cb;
				
				return true;
			}
			
			return false;
		},
	
		/**
		 * @typedef $s.mutationObserver.remove
		 */
		remove: function(name)
		{
			if (typeof this._registeredCallbacks[name] === "function") {
				delete this._registeredCallbacks[name];
				
				return true;
			}
			 
			return false;
		},
		
		init: function()
		{
			const _that = this;
			const _observerCaller = function (mutations) {
				if (_that._registeredCallbacks !== {}) {
					for (const i in _that._registeredCallbacks) {
						try {
							_that._registeredCallbacks[i](mutations);
						} catch (e) {
							console.error('mutationObserver: Error in cb "' + i + '":');
							console.error(e);
						}
					}
				}
			};
			
			const mutationObserver = window.MutationObserver || window.WebKitMutationObserver;
			if (mutationObserver) {
				new mutationObserver(_observerCaller).observe(window.document.body, { attributes: true, childList: true, subtree: true });
			} else if (window.addEventListener){
				window.body.addEventListener('DOMNodeInserted', _observerCaller, false);
				window.body.addEventListener('DOMNodeRemoved', _observerCaller, false);
			} else {
				console.warn("mutationObserver can't be initialized; no API available.");
			}
		}
	}
});