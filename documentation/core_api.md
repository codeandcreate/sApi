# sAPI Core
In its core all is about extending :)

## extend()
``` js
$s.extend({
	myExtension: {
		_objectReady: false, //optional if a function wants to check if init was called...
	
		myFunction: function(param)
		{
			// Just a sample how to check if i'm ready...
			if (this._objectReady !== false) {
				//...
			}
		},
		
		init: function()
		{
			// some stuff i want to execute in the moment this object will be added to sApi
		}
	}
});
```

## onApiReady()
Checks if $s has an extension loaded and executes something as soon as the extension is ready to go:
``` js
$s.onApiReady("myExtension", function()
{
	$s.myExtension.myFunction("something");
});
```
Also works with "myExtension.subExtension" if needed.

## Module: ready()
Execute something on/after document ready:
``` js
$s.ready(function() {
	console.log("document is ready");					
}
```

## Module: qs()
It's a shortcut for document.querySelectorAll():
``` js
let myNode = $s.qs(".classOfMyNode");

```

But is has some benefits. For example it uses _getElementById_ if it detects that you want to get just the object by its id. It also makes working with a array of objects a bit comfortable:

``` js
const aListOfNodes = $s.qs(".btn");
if (aListOfNodes !== null) {
	var isAList = aListOfNodes.isList;

	//...

	aListOfNodes.forEach(function(oneOfThisNodes, index, array) 
	{
		oneOfThisNodes.removeAttribute("onclick");
		oneOfThisNodes.addEventListener("click", function() 
		{
			console.log("You have clicked on the button with the index " + index);
		});
	});
} 
```

The returning Object also includes some helpers like seen above:

### node.isList
A boolean if the returned Node is a nodeList or a single dom node.

### node.items
A nodeList if ```node.isList``` is true.

### node.forEach
Helper to go through all nodes if ```node.items```.

### node.onNextSibling()
Executes a callback on the next sibling of ```node```:

``` js
$s.qs("#myNode").onNextSibling(
	(sibling) => {
		console.log(sibling)
	}
);
```

### node.onParentOfType()
Executes a callback on the first parent node of a given type:
``` html
<main>
	<section>
		<ul>
			<li>
				<a href="#..." class="link">...</a>
			</li>
		</ul>
	</section>
</main>
```
``` js
$s.qs(".link").onParentOfType(
	"SECTION" // needs to be upper case because the function uses node.nodeName
	(sibling) => {
		// shows the section of the above structure
		console.log(sibling) 
	}
);
```

### node.onParentOfClass()
Same as ```node.onParentOfType``` but based on a class name of a parent node.

## Module: genNodes()
A simple DOM structure generator
``` js
const myNewStructure = $s.genNodes(
	[	// First a array of elements
		[	// Each element has a type; the forst one is a h1:
			'h1',
			{	// Attributes are optional and can be empty.
				// Each attribute can be text or a function.
				innerText: "Headline",
				// A function will be added with "addEventListener". For example:
				click: (event) => {
					console.log("You clicked at the headline...");
				}
			}
		],
		[
			'section',
			{
				className: "content",
				// the attributes-key is handled differently:
				// all inside this object will be added as attributes no matter what string is used as key
				attributes: {
					'data-module': "article",
					'data-id': 1234
				}
			},
			[
				[
					'p',
					{
						innerText: "Lorem ipsum,..."
					}
				],
				[
					'p',
					{
						innerText: "... next paragraph."
					}
				]
			]
		]
	],
	"article", // optional, defaults to "div"
	// Also optional, defaults to {}:
	{
		className: "teaser"
	}
);	
```
This above generates:
``` html
<article class="teaser">
	<h1>Headline</h1>
	<section class="content" data-module="article" data-id="1234">
		<p>Lorem ipsum,...</p>
		<p>... next paragraph.</p>
	</section>
</article>
```

## Module: ajax()
Load something on runtime:

``` js
$s.ajax({
	url:		"/testscript.php", //required
	method:		"GET",
	data:		{},
	headers: {},
	withCredentials: true,
	callback:	function( returnData, httpStatus ) { //required
		console.log(returnData);					
	},
	errorCallback:	function( XMLHttpRequest ) {
		console.log(XMLHttpRequest);
	},
	onProgressCallback:	function( XMLHttpRequest ) {
		console.log(XMLHttpRequest);
	}
});	
```

## Module: External Module Loader
This is a transparent extension that creates dummy objects. On executing such a function of a dummy object it loads the javascript that contains the real extension and executes the real function after loading.

To use the module you need to make an initial configuration. This configuration can placed inside the compiled sApi itself or before the api is loaded inside the dom. For the first option add your configuration to sApi_pre/00_init.js before building. A example for the second can be found in /demos/misc/misc.html

### Configuration example
``` js
const _emlConfig = {
	// Name of the object that needs to be created
	'frontendLogger': {              
		// URL of the real object that needs to be loaded           
		'src': "../../tools/s_frontendLogger.js",
		// List of function names that trigger the loading of the script
		'fns': [                                  
				'log'
		]
	}
};
```