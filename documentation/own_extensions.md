# Writing own extensions
The basic structure of a sApi extension is always calling ```extend()``` with an object. If you use another shortcut than ```$s``` for your api you must keep in mind, that if you pack an extension you need to use ```$s``` anyway. But if you load it at runtime you must use your own prefix.

``` js
$s.extend({
	myExtension: { // this could also be a function!
		//...
	}
});
```

## init() and _objectReady
With adding your object to the api, a variable is placed inside of it: ```_objectReady```. If you define a function named ```init``` in your object, it will be called automatically. After calling it, ```_objectReady``` changes from ```false``` to ```true```. This variable is used by ```$s.onApiReady()``` to check if an object (or function) is there and "ready to use". 

``` js
$s.extend({
	myExtension: {
		someFunction: function()
		{
			//...
		},
		
		init: function()
		{
			//...
		}
	}
});
```

If you set ```_objectReady: false``` for your own, ```extend()``` will not automatically change it to ```true```. You need to do it manually. This is useful if your initialization runs async; needs some callbacks or a promise (...).

``` js
$s.extend({
	myExtension: {
		// will never change if you don't do it yourself...
		_objectReady: false,
		
		someFunction: function(callback)
		{
			// do something async...
			
			callback();
		},
		
		init: function()
		{
			const that = this;
			this.someFunction(function()
			{
				that._objectReady = true;
			});
		}
	}
});
```

## There are always parents
You shouldn't call some other functions of sApi inside by using ```$s```. Because you need some reference to other levels of the api, there is ```_parent``` in all objects of sApi that points to the level above:

``` js
$s.extend({
	myExtension: {
		mySubExtension: {
			someSubFunction: function()
			{
				// calls your $s.myExtension.someFunction();
				this._parent.someFunction(); 
				
				// calls $s.ajax()
				this._parent._parent.ajax({
					//...
				});
			}
		},
		
		someFunction: function()
		{
			// calls $s.require.js()
			this._parent.require.js("...");
		},
		
		init: function()
		{
			//...
		}
	}
});
```