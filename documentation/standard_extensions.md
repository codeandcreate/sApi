# Standard Extensions
Instead of packing all extensions with the core sApi.min.js by putting them in sApi_modules there are several default extensions that can be loaded on runtime with the help of the __External Module Loader__.

This requires to set up a _emlConfig with something like this:
``` js
const _emlConfig = {
	'mutationObserver': {                        
		'src': "/path/to/my/js/s_mutationObserver.js",
		'fns': [
			'add', 'remove'
		]
	},
	'replaceWith': {
		'src': "/path/to/my/js/s_replaceWith.js",
		'fns': [
			'mixedNodes', 'iframe', 'image', 'embed', 'script', 'elementInsteadOf'
		]
	},
	'require': {
		'src': "/path/to/my/js/s_require.js",
		'fns': [
			'js', 'css', 'object'
		]
	}
};
```

## Module: mutationObserver{}
A little Wrapper around MutationObserver on document.body with fallback for older browsers.

### add()
Adds a function to the observer if not already set
``` js
$s.mutationObserver.add("someCaption", () => { /* ... */ });
```

### remove()
Removes a observer if set
``` js
$s.mutationObserver.remove("someCaption");
```

## Module: replaceWith{}
If you need to replace the current &gt;script&lt;-Tag with something, thats the Object for you.

### mixedNodes()
Replaces content of the current node with base64 data
``` html
<script>
	$s.replaceWith.mixedNodes("[base64 encoded data]");
</script>
```

### iframe(), image() and embed()
Replace content of the current node with an iFrame, embed or image
``` html
<script>
	$s.replaceWith.iframe("url", {"width": "...", "andOther": "attributes"});
</script>
```

### script()
Similar to iframe() (...) but with the option not to only load an external javascript
``` html
<script>
	$s.replaceWith.script('console.log("hello world");', {"some": "attributes"});
</script>
```

### elementInsteadOf()
Base for mixedNodes(), iframe(), image(), embed() and script()
``` html
<script>
	$s.replaceWith.elementInsteadOf(document.currentScript, "script", {"src": "/my_scripts/script.js"});
</script>
```

## Module: require{}
Module to load and/or wait for Styles, Javascript or Objects

### require.js() / require.css()
Load an other javascript or stylesheet file:
``` js
$s.require.js("/js/somejsfile.js", function() {
	console.log("js file is loaded.");			
});

$s.require.css("/css/somecssfile.css", function() {
	console.log("css file is loaded.");					
});
```

You can also load javascripts on a more extended way; for example load more than one ...
``` js
$s.require.js(
	[
		"/js/somejsfile1.js",
		"/js/somejsfile2.js"
	], 
	() => {
		console.log("js files is loaded.");			
	}
);
```
... or even wait for some objects (_objectName_) or sApi extensions (_apiName_) to load:
``` js
$s.require.js(
	[
		{
			url:			"/js/somejsfile1.js",
			apiName:	"someApi1"
		},
		{
			url:			"/js/somejsfile2.js",
			apiName:	"someApi2"
		}
	], 
	() => {
		console.log("js files is loaded.");			
	}
);
```
If you use such a construct above, all files will be loaded after the parent and only if the api or object is present. The final callback will be loaded if _$s.someApi2_ is ready.

### require.object()
Waits till a javascript object (outside of sApi) is loaded:
``` js
$s.require.object(
	"javascriptObjectName",
	function() 
	{
		// some callback stuff
	},
	"optionalListenerName" // if param is not set, there runs a time in background to wait for javascriptObjectName.
);
```