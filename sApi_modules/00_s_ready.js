/**
 * a $( document ).ready({});-alike
 * Forked from https://github.com/jfriend00/docReady
 * It uses d and w of the $s-Api initial params.
 */
$s.extend({
	_docReadyList                   : [],
	_docReadyFired                  : false,
	_docReadyEventHandlersInstalled : false,

	/**
	 * Calls callback if document is ready
	 *
	 * @typedef $s.ready
	 * @param callback   that will be fired if document is ready
	 * @param context    optional context
	 */
	ready: function (callback, context)
	{
		const that = this;
		/**
		 * Belongs to _docReady
		 */
		function _documentIsReady()
		{
			if (!that._docReadyFired) {
				that._docReadyFired = true;
				for (let i = 0; i < that._docReadyList.length; i++) {
					try {
						that._docReadyList[i].fn.call(w, that._docReadyList[i].ctx);
					} catch (error) {
						console.warn(error);
					}
				}
				that._docReadyList = [];
			}
		}

		/**
		 * Belongs to _docReady
		 */
		function _readyStateChange()
		{
			if (document.readyState === "complete") {
				_documentIsReady();
			}
		}

		if (this._docReadyFired) {
			setTimeout(() => {
				try {
					callback(context);
				} catch (error) {
					console.warn(error);
				}
			}, 1);
			return;
		} else {
			this._docReadyList.push({fn: callback, ctx: context});
		}
		if (d.readyState === "complete") {
			setTimeout(_documentIsReady, 1);
		} else if (!this._docReadyEventHandlersInstalled) {
			if (d.addEventListener) {
				d.addEventListener("DOMContentLoaded", _documentIsReady, false);
				w.addEventListener("load", _documentIsReady, false);
			} else {
				document.attachEvent("onreadystatechange", _readyStateChange);
				w.attachEvent("onload", _documentIsReady);
			}
			this._docReadyEventHandlersInstalled = true;
		}
	}
});