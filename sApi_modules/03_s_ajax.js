/**
 * $s.ajax() as sApi Extension
 *
 */
$s.extend({
	/**
	 * Shortcut function for XMLHttpRequest
	 *
	 * @typedef $s.ajax
	 * @param urlOrObject
	 * @param callback
	 * @param data
	 * @param method
	 * @returns {boolean}
	 */
	ajax: function (urlOrObject, callback, data, method) {
		let errorCallback, onProgressCallback, headers, withCredentials;

		if (typeof urlOrObject === "object") {
			data = urlOrObject.data || "";
			method = urlOrObject.method || "GET";
			callback = urlOrObject.callback || undefined;
			errorCallback = urlOrObject.errorCallback || undefined;
			onProgressCallback = urlOrObject.onProgressCallback || undefined;
			headers = urlOrObject.headers || {};
			withCredentials = urlOrObject.withCredentials || undefined;

			urlOrObject = urlOrObject.url || undefined;
		}

		if (typeof urlOrObject === "undefined" || typeof callback === "undefined") {
			return false;
		}

		if (typeof window.sApi_jsHost !== "undefined" && urlOrObject.indexOf("://") === -1) {
			urlOrObject = window.sApi_jsHost + urlOrObject;
		}

		// filter methods
		if (["PUT", "HEAD", "POST", "PUT", "DELETE", "CONNECT", "OPTIONS", "TRACE", "PATCH"].indexOf(method) === -1) {
			method = "GET";
		}

		// data preparation
		if (typeof data === "object") {
			let dataObject = data;
			data = [];
			for (const key in dataObject) {
				data.push(key + "=" + encodeURIComponent(dataObject[key]));
			}
			data = data.join("&");
		} else if (typeof data === "undefined") {
			data = "";
		}

		const xmlHttp = new XMLHttpRequest();
		if (withCredentials === true) {
			xmlHttp.withCredentials = true;
		}

		xmlHttp.onload = function () {
			if (xmlHttp.readyState === 4 && xmlHttp.status < 400) {
				callback(xmlHttp.responseText, xmlHttp.status);
			}
		};

		if (typeof onProgressCallback === "function") {
			xmlHttp.onprogress = onProgressCallback(xmlHttp);
		}

		xmlHttp.onreadystatechange = function () {
			if (xmlHttp.readyState === 4 && xmlHttp.status >= 400 && typeof errorCallback === "function") {
				errorCallback(xmlHttp);
			}
		};

		if (["PUT", "POST", "PATCH"].indexOf(method) !== -1) {
			// with post data
			xmlHttp.open(method, urlOrObject, true);
			xmlHttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			if (typeof headers === "object") {
				for (const i in headers) {
					xmlHttp.setRequestHeader(i, headers[i]);
				}
			}
			xmlHttp.send(data);
		} else {
			// with get data
			if (data !== "") {
				data = "?" + data;
			}
			xmlHttp.open(method, urlOrObject + data, true);
			if (typeof headers === "object") {
				for (const i in headers) {
					xmlHttp.setRequestHeader(i, headers[i]);
				}
			}
			xmlHttp.send();
		}

		return true;
	}
});