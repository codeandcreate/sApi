/**
 * External Module Loader
 *
 * Loads a sApi extension on first running one of its functions.
 * To create dummy loader objects this loader needs a configuration.
 * It needs $s.ajax to operate
 *
 */
$s.extend({
	/**
	 * Has a "_" because it shouldn't be worked with manually.
	 */
	_eml: 
	{
		// Holds the config that must be in window._emlConfig on page load.
		config: {},
		// The "real object loader" load the javascript from .src and makes sure
		// to execute all calls after the module is loaded via the little function
		// registry inside the current module configuration (.initFunctions)
		roL: function(obj, fn, params) 
		{
			const that = this;

			if (!this._parent[obj]._isEml || this._parent[obj]._isLoading) {
				if (fn === null) {
					this.config[obj].initFunctions.push(function() {
						that._parent[obj].apply(that._parent[obj], params);
					});
				} else {
					this.config[obj].initFunctions.push(function() {
						that._parent[obj][fn].apply(that._parent[obj], params);
					});
				}
			} else {
				this._parent[obj]._isLoading = true;
				this._parent[obj]._objectReady = false;
				
				this._parent.ajax({
					'url': this._parent._eml.config[obj].src,
					'callback': function(data) {
						if (fn === null) {
							that._parent._eml.config[obj].initFunctions.push(function() {
								that._parent[obj].apply(that._parent[obj], params);
							});
						} else {
							that._parent._eml.config[obj].initFunctions.push(function() {
								that._parent[obj][fn].apply(that._parent[obj], params);
							});
						}
						delete(that._parent[obj]);

						that._parent.onApiReady(obj, () => {
							for (const i in that._parent._eml.config[obj].initFunctions) {
								if (!isNaN(i) && typeof that._parent._eml.config[obj].initFunctions[i] === "function") {
									that._parent._eml.config[obj].initFunctions[i]();
									delete(that._parent._eml.config[obj].initFunctions[i]);
								}
							}
						});
						
						eval(data);
					}
				});
			}
		},
		init: function() 
		{
			if (typeof _emlConfig === "object") {
				// Import the _emlConfig
				this.config = _emlConfig;
				delete(_emlConfig);
				
				// go through all configured objects and make dummys
				for (const objectName in this.config) {
					// we need a placeholder for functions that run after loading the real one
					this.config[objectName].initFunctions = [];
					// then we need a container
					const _tmp = {};

					// do we have a single function or an object?
					if (this.config[objectName].fns.length === 0) {
						// creating a dummy function for the function (~object) name
						_tmp[objectName] = function() {
							this._eml.roL(objectName,null,arguments);
						}
					} else {
						// creating dummy functions that load the real object on call
						_tmp[objectName] = {};
						for (const fni in this.config[objectName].fns) {
							const functionName = this.config[objectName].fns[fni];
							_tmp[objectName][functionName] = function() {
								this._parent._eml.roL(objectName,functionName,arguments);
							}
						}
					}
					
					// some default vars to detect what's going on
					_tmp[objectName]._isEml = true;
					_tmp[objectName]._isLoading = false;

					// lets extend sApi with our dummy
					this._parent.extend(_tmp);
				}
			}
		}
	}
});