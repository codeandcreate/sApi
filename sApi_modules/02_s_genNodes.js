$s.extend({
	/**
	 * A simple DOM generator
	 *
	 * @typedef $s.genNodes
	 * @param structToGen
	 * @param container optional
	 * @returns {*}
	 */
	genNodes: function(structToGen, container, attributes)
	{
		function _gen(struct)
		{
			const elements = [];
			for (const el in struct) {
				let newEl = document.createElement(struct[el][0]);
				for (const attr in struct[el][1]) {
					if (typeof struct[el][1][attr] === "function") {
						newEl.addEventListener(attr, struct[el][1][attr])
					} else if (attr === "attributes" && typeof struct[el][1][attr] === "object") {
						for (const attributeName in struct[el][1][attr]) {
							newEl.setAttribute(attributeName, struct[el][1][attr][attributeName]);
						}
					} else {
						newEl[attr] = struct[el][1][attr];
					}
				}
				if (Array.isArray(struct[el][2])) {
					const newElements = _gen(struct[el][2]);
					if (Array.isArray(newElements)) {
						for (const i in newElements) {
							newEl.appendChild(newElements[i]);
						}
					} else {
						newEl.appendChild(newElements);
					}
				}
				elements.push(newEl);
			}

			return elements.length == 1 ? elements[0] : elements;
		}

		return _gen([
			[
				container || "DIV",
				attributes || { },
				structToGen
			]
		]);
	}
});
