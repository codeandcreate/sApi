#!/bin/bash
if [ -z "$1" ]; then
    echo "Error: File/path of api to build not given"
    exit
fi
if [ ! -e "$1" ]; then
	echo "Error: File not found"
	exit
fi

inputFileName=$1
baseApiFileName=$(basename $inputFileName)
baseApiFileName="${baseApiFileName:0:${#baseApiFileName}-3}"
basePath=$(dirname $inputFileName)/
modulePath=$basePath$baseApiFileName'_modules/'
moduleData=""
extensionPath='extensions/'
prePath=$basePath$baseApiFileName'_pre/'
preData=""

if [ ! -e $modulePath ]; then
	echo "Error: The modulepath $modulePath doesn't exists"
	exit
fi

if [ ! -e $prePath ]; then
	echo "Error: The prepath $prePath doesn't exists"
	exit
fi

if [ ! -d build ]; then
	mkdir build
fi

if [ ! -d "build/$extensionPath" ]; then
	mkdir "build/$extensionPath"
fi

echo "Building $baseApiFileName..."

cd "$basePath"

for filename in $(ls $prePath/*.js); do
	tmp=`cat $filename`
	echo "- included pre javascript: $(basename $filename)"
	preData="$preData$tmp"
done

for filename in $(ls $modulePath/*.js); do
	tmp=`cat $filename`
	tmp="${tmp/\$s.extend({/}"
	tmp="${tmp:0:${#tmp}-3}"
	echo "- included module: $(basename $filename)"
	moduleData="$moduleData$tmp,"
done

baseApi=`cat $baseApiFileName.js`
baseApi="${baseApi/'_includeModulesPlaceHolder: {},'/$moduleData}"

echo "$preData" > $baseApiFileName.min.js
echo "$baseApi" >> $baseApiFileName.min.js

mv $baseApiFileName.min.js build/$baseApiFileName.min.js

echo "Build done. compressing..."
if hash php 2>/dev/null && [ -f "./tools/packer/class.JavaScriptPacker.php" ];  then
  echo "- using php / packer"
  php ./tools/packer/packer.php build/$baseApiFileName.min.js
	echo "build/$baseApiFileName.min.js compressed"
elif hash yuicompressor 2>/dev/null; then
  echo "- using yuicompressor"
  yuicompressor build/$baseApiFileName.min.js -o build/$baseApiFileName.min.js
	echo "build/$baseApiFileName.min.js compressed"
elif hash yui-compressor 2>/dev/null; then
  echo "- using yui-compressor"
  yui-compressor build/$baseApiFileName.min.js -o build/$baseApiFileName.min.js
	echo "build/$baseApiFileName.min.js compressed"
else
  echo "- Warning: no compressor found..."
fi

echo "Compressing extensions..."
for extensionFile in $(ls $extensionPath/*.js); do
	cp $extensionFile build/$extensionFile
	if hash php 2>/dev/null && [ -f "./tools/packer/class.JavaScriptPacker.php" ];  then
		echo "- $(basename $extensionFile)"
		php ./tools/packer/packer.php build/$extensionFile
	elif hash yuicompressor 2>/dev/null; then
		echo "- $(basename $extensionFile)"
		yuicompressor build/$extensionFile -o build/$extensionFile
	elif hash yui-compressor 2>/dev/null; then
	echo "- $(basename $extensionFile)"
		yui-compressor build/$extensionFile -o build/$extensionFile
	else
		echo "- Warning: no compressor found..."
	fi
done

if [ -f tools/_emlConfig_example.js ]; then
	cp tools/_emlConfig_example.js build/
fi