# sApi - Small Javascript Api
A minimalistic javascript api: The core is a little javascript that can extend itself with other objects or functions by calling ```$s.extend()```. If an extension should be packed within the minifying, it must be just placed into sApi_modules.

## Changes

|&nbsp;&nbsp;&nbsp;Version&nbsp;&nbsp;&nbsp;| Info / Highlights |
|---|---|
| 202308.02 | genNodes attributes-workflow, ajax fixes  |
| 202307.01 | genNodes optimized, ajax refactored, new ajax demo |
| 202210.01 | Small fixes | 
| 202209.01 | Restructuring of $s.dom to $s.replaceWith and $s.genNodes, mutationObserver-Wrapper, default _emlConfig, EML can now load function only extensions | 
| 202208.02 | added $s.dom.gen to generate dom structures | 
| 202208.01 | _parent, loading of more than one file on $s.require.js / $s.require.css | 
| 202207.01 | "External Module Loader", documentation update and some fixes |
| 202206.01 | Bugfixes/typos |
| 202203.01 | Sync with internal version. Mostly bug fixes |
| 202112.01 | Init for bundled sApi_modules, bugfix for $s.require.object() |
| 202012.01 | First public version of sApi, based on smdQS/smdApi from Schwäbisch Media Digital |
| 202106.01 | Some little fixes |

## Documentation
- [Core API](documentation/core_api.md)
- [Standard Extensions](documentation/standard_extensions.md)
- [About writing own extensions](documentation/own_extensions.md)

## sApi_jsHost
If the javascript variable sApi_jsHost is set, $s.ajax() and $s.require.js() uses it as hostname to call all requests to. If a url has already a hostname the variable is ignored.

## Packer and building
The default minifing is done by packer (http://dean.edwards.name/packer/). To make own minified build either install yui compressor or place the packer php file from http://joliclic.free.fr/php/javascript-packer/en/ into tools/packer.

To pack (and optionally minify) sApi: ```bash build.sh sApi.js```

## Demos
You need to run the included demo on a web server. The simplest way to do it is to run it via the php build in web server. Open a terminal and go to the root directory of this code, build the code or place the sApi.min.js next to the sApi.js and enter: ```php -S localhost:8000```

Now you can open the Demo in a web browser via the url http://localhost:8000/demos/misc/misc.html

## Misc
This version includes a forked version of docReady (https://github.com/jfriend00/docReady)
