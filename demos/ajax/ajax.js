$s.ready(() => {
	let intervalIndex = 0;
	let interval = setInterval(function() {
		switch (intervalIndex) {
			case 0:
				$s.frontendLogger.log('Let\'s start!\n');
				break;
			case 1:
				$s.ajax({
					url: "ajax.php",
					callback: (raw, http) => {
						$s.frontendLogger.log('Request: \n{\n	url: "ajax.php",\n	callback: (raw) => { /*...*/ }\n}');
						$s.frontendLogger.log('Return (' + http + '): ' + raw + "\n");
					}, 
					errorCallback: (xmlHttpR) => {
						$s.frontendLogger.log("Return error http status: " + xmlHttpR.status);
					}
				});
				break;
			case 2:
				$s.ajax({
					url: "ajax.php",
					method: "POST",
					data: {
						name: "value"
					},
					callback: (raw, http) => {
						$s.frontendLogger.log('Request: \n{\n	url: "ajax.php",\n	method: "POST"\n	data: {\n		name: "value"\n	},\n	callback: (raw) => { /*...*/ }\n}');
						$s.frontendLogger.log('Return (' + http + '): ' + raw + "\n");
					}, 
					errorCallback: (xmlHttpR) => {
						$s.frontendLogger.log("Return error http status: " + xmlHttpR.status);
					}
				});
				break;
			case 3:
				$s.ajax({
					url: "ajax.php",
					method: "PUT",
					data: {
						name: "value",
						int: 1,
						json: {
							just: "a",
							sub: {
								obj: "with",
								structure: "and a",
								array: [
									1, 2, 3
								]
							}
						}
					},
					callback: (raw, http) => {
						$s.frontendLogger.log('Request: \n{\n	url: "ajax.php",\n	method: "PUT"\n	data: {\n		name: "value",\n		int: 1,\n		json: {\n			just: "a",\n			sub: {\n				obj: "with",\n				structure: "and a",\n				array: [\n					1, 2, 3\n				]\n			}\n		}\n	},\n	callback: (raw) => { /*...*/ }\n}');
						$s.frontendLogger.log('Return (' + http + '): ' + raw + "\n");
					}, 
					errorCallback: (xmlHttpR) => {
						$s.frontendLogger.log("Return error http status: " + xmlHttpR.status);
					}
				});
				break;
			case 4:
				$s.ajax({
					url: "ajax.php",
					method: "DELETE",
					data: {
						param: "val",
						structure: [
							1, 2, 3
						]
					},
					callback: (raw, http) => {
						$s.frontendLogger.log('Request: \n{\n	url: "ajax.php",\n	method: "DELETE"\n	data: {\n		param: "val",\n		structure: [\n			1, 2, 3\		\n		]\n	},\n	callback: (raw) => { /*...*/ }\n}');
						$s.frontendLogger.log('Return (' + http + '): ' + raw + "\n");
					}, 
					errorCallback: (xmlHttpR) => {
						$s.frontendLogger.log("Return error http status: " + xmlHttpR.status);
					}
				});
				break;
			case 5:
				$s.ajax({
					url: "ajax.php2",
					callback: (raw, http) => {},
					errorCallback: (xmlHttpR) => {
						$s.frontendLogger.log('Request: \n{\n	url: "ajax.php2",\n	method: "GET",\n	callback: (raw) => { /*...*/ }\n}');
						$s.frontendLogger.log("Return http error status: " + xmlHttpR.status);
					}
				});
				break;	
			default:
				clearInterval(interval);
		}
		
		
		console.log(intervalIndex);
		
		intervalIndex += 1;
	}, 1000);
});