<?php

// process input stream
$phpInputStream = fopen("php://input","r");
$inputData = "";
while ($data = fread($phpInputStream,1024))
	$inputData .= $data;
fclose($phpInputStream);

echo json_encode(
	[
		'REQUEST' => basename($_SERVER['SCRIPT_NAME']),
		'REQUEST_METHOD' => $_SERVER['REQUEST_METHOD'],
		'GET' => $_GET,
		'POST' => $_POST,
		'RAW' => $inputData
	]
);