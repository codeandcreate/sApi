# How to run the demos
The recommended way to run these demos is to use the development web server that is build into PHP.

1. Install PHP
2. Run following command in the root dir of this project (a level up to this file): php -S localhost:8080
3. Open the demos in a browser via http://localhost:8080/demos/ajax/ajax.html or http://localhost:8080/demos/misc/misc.html